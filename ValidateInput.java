public class ValidateInput{

	//Validasi firstName
	public static boolean validateFirstName(String firstName){
		return firstName.matches("[A-Z] [a-zA-Z*");
	}

	//Validasi LastName
	public static boolean validateLastName(String lastName){
		return lastName.matches("[a-zA-z]+ (['-][a-zA-Z]+)*");
	}

	//Validasi Alamat
	public static boolean validateAddress(String address){
		return address.matches("\\d+\\s+([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)");
	}

	//Validasi kota
	public static boolean validateCity(String city){
		return city.matches("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)");
	}

	//Validasi negara
	public static boolean validateState(String state){
		return state.matches("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)");
	}
	//Validasi KodePos
	public static boolean validateZip(String zip){
		return zip.matches("\\{5}");
	}
	//Validasi telephone
	public static boolean validatePhone(String phone){
		return phone.matches("[1-9]\\d{2}-[1-9]\\d{2}-\\d{4}");
	}

}