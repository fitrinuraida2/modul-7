import java.util.Date; 
import java.text.ParseException; 
import java.text.SimpleDateFormat; 

public class CompareToDate { 
    public static void main(String args[]) 
        throws ParseException 
    { 
        SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd"); 

        Date d1 = sdfo.parse("2018-03-31"); 
        Date d2 = sdfo.parse("2012-03-31"); 

        System.out.println("Date1 : " + sdfo.format(d1)); 
        System.out.println("Date2 : " + sdfo.format(d2)); 

        if (d1.compareTo(d2) > 0) { 

            System.out.println("Date1 is after Date2"); 
        } 

        else if (d1.compareTo(d2) < 0) { 

            System.out.println("Date1 is before Date2"); 
        } 

        else if (d1.compareTo(d2) == 0) { 

            System.out.println("Date1 is equal to Date2"); 
        } 
    } 
}
